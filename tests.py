import unittest
import event_db
import schedule_db
import todolist
import bot
import os
import psycopg2

DATABASE_URL = os.environ['DATABASE_URL']
__connection = None


def get_connection():
    global __connection
    if __connection is None:
        __connection = psycopg2.connect(DATABASE_URL, sslmode='require')
    return __connection


class TestFunctions(unittest.TestCase):

    def test_code_converter(self):
        res = schedule_db.code_converter('12:00:00')
        self.assertEqual(res, 3)

    def test_show_schedule(self):
        conn = get_connection()
        c = conn.cursor()
        sql = '''SELECT to_char(lt.lesson_time, 'HH24:MI'), s.lesson, s.lecture_hall FROM schedule s 
        JOIN lesson_times lt ON s.lesson_number = lt.lesson_number 
        WHERE (s.u_id, s.day) = (%s, %s) ORDER BY s.lesson_number;'''
        c.execute(sql, (73642841, 1))
        res = schedule_db.show_schedule(u_id=73642841, day=1)
        self.assertEqual(res, c.fetchall())

    def test_show_time_list(self):
        conn = get_connection()
        c = conn.cursor()
        sql = '''SELECT lesson_number FROM schedule 
        WHERE (u_id, day) = (%s, %s) ORDER BY lesson_number;'''
        c.execute(sql, (73642841, 1))
        res = schedule_db.show_time_list(u_id=73642841, day=1)
        self.assertEqual(res, c.fetchall())

    def test_show_events(self):
        conn = get_connection()
        c = conn.cursor()
        sql = "SELECT event, to_char(time, 'DD.MM.YY HH24:MI') FROM events WHERE u_id = %s ORDER BY id DESC"
        c.execute(sql, (73642841,))
        res = event_db.show_events(u_id=73642841)
        self.assertEqual(res, c.fetchall())

    def test_show_event_list(self):
        conn = get_connection()
        c = conn.cursor()
        sql = 'SELECT event FROM events WHERE u_id = %s ORDER BY id DESC'
        c.execute(sql, (73642841,))
        res = event_db.show_event_list(u_id=73642841)
        self.assertEqual(res, c.fetchall())

    def test_show_list(self):
        conn = get_connection()
        c = conn.cursor()
        sql = 'SELECT task FROM to_do_list WHERE u_id = %s ORDER BY id DESC'
        c.execute(sql, (73642841,))
        res = todolist.show_list(u_id=73642841)
        self.assertEqual(res, c.fetchall())

    def test_show_class(self):
        res = bot.show_class(73642841, 6)
        check = '12:00 - Test, ISIT'
        self.assertEqual(res, check)


if __name__ == '__main__':
        unittest.main()