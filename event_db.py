import os
import psycopg2

DATABASE_URL = os.environ['DATABASE_URL']


def ensure_connection(func):
    def inner(*args, **kwargs):
        with psycopg2.connect(DATABASE_URL, sslmode='require') as con:
            res = func(*args, con=con, **kwargs)
        return res
    return inner


@ensure_connection
def init_table(con, force: bool = False):
    c = con.cursor()

    if force:
        c.execute('DROP TABLE IF EXISTS events;')

    c.execute('''
        CREATE TABLE IF NOT EXISTS events (
            id serial PRIMARY KEY,
            u_id integer NOT NULL,
            event text NOT NULL,
            time timestamptz NOT NULL
        );
    ''')
    con.commit()


@ensure_connection
def add_event(con, u_id: int, event: str, time: str):
    cursor = con.cursor()
    cursor.execute('INSERT INTO events (u_id, event, time) VALUES (%s, %s, %s);', (u_id, event, time))
    con.commit()


@ensure_connection
def del_event(con, u_id: int, event: str):
    cursor = con.cursor()
    cursor.execute('DELETE FROM events WHERE (u_id, event) = (%s, %s)', (u_id, event))
    con.commit()


@ensure_connection
def show_events(con, u_id: int):
    cursor = con.cursor()
    cursor.execute("SELECT event, to_char(time, 'DD.MM.YY HH24:MI') FROM events WHERE u_id = %s ORDER BY id DESC", (u_id,))
    return cursor.fetchall()


@ensure_connection
def show_event_list(con, u_id: int):
    cursor = con.cursor()
    cursor.execute('SELECT event FROM events WHERE u_id = %s ORDER BY id DESC', (u_id,))
    return cursor.fetchall()


if __name__ == '__main__':
    u_id = 12345
    event = "meet at restaurant"
    time = "29 apr 12:00"
    ed = "meet at cafe"
    edt = "30 may 21:00"
    event_list = show_events(u_id=u_id)
    text = '\n'.join([f'{eve}, {time}' for eve, time in event_list])
    print(text)
