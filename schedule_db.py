import psycopg2
import os

DATABASE_URL = os.environ['DATABASE_URL']


def ensure_connection(func):
    def inner(*args, **kwargs):
        with psycopg2.connect(DATABASE_URL, sslmode='require') as con:
            res = func(*args, con=con, **kwargs)
        return res
    return inner


@ensure_connection
def init_table(con, force: bool = False):
    c = con.cursor()

    if force:
        c.execute('DROP TABLE IF EXISTS lesson_times;')
        c.execute('DROP TABLE IF EXISTS schedule;')

    c.execute('''
        CREATE TABLE IF NOT EXISTS lesson_times (
            lesson_number integer PRIMARY KEY,
            lesson_time time NOT NULL,
            CHECK (lesson_number >= 1 AND lesson_number <=7));
    ''')
    c.execute('''
        INSERT INTO lesson_times VALUES 
            (1, '8:30'),
            (2, '10:15'),
            (3, '12:00'),
            (4, '14:10'),
            (5, '15:55'),
            (6, '17:40'),
            (7, '19:25')
            ON CONFLICT DO NOTHING;
    ''')
    c.execute('''
        CREATE TABLE IF NOT EXISTS schedule (
            id serial PRIMARY KEY,
            u_id integer NOT NULL,
            day integer NOT NULL,
            lesson_number integer NOT NULL,
            lecture_hall text NOT NULL,
            lesson text NOT NULL,
            CHECK (day >=1 AND day <=7),
            FOREIGN KEY (lesson_number) REFERENCES lesson_times (lesson_number) ON DELETE CASCADE);
    ''')
    con.commit()


@ensure_connection
def add_user(con, u_id: int):
    cursor = con.cursor()
    cursor.execute('INSERT INTO schedule (u_id) VALUES (%s);', (u_id,))
    con.commit()


@ensure_connection
def add_class(con, u_id: int, day: int, lesson_num: int, lecture_hall: str, lesson: str):
    cursor = con.cursor()
    cursor.execute('INSERT INTO schedule (u_id, day, lesson_number, lecture_hall, lesson) VALUES (%s, %s, %s, %s, %s);',
                   (u_id, day, lesson_num, lecture_hall, lesson))
    con.commit()


@ensure_connection
def del_class(con, u_id: int, day: int, lesson_num: int):
    cursor = con.cursor()
    cursor.execute('DELETE FROM schedule WHERE (u_id, day, lesson_number) = (%s, %s, %s);', (u_id, day, lesson_num))
    con.commit()


@ensure_connection
def show_schedule(con, u_id: int, day: int):
    cursor = con.cursor()
    cursor.execute('''
        SELECT to_char(lt.lesson_time, 'HH24:MI'), s.lesson, s.lecture_hall FROM schedule s 
        JOIN lesson_times lt ON s.lesson_number = lt.lesson_number 
        WHERE (s.u_id, s.day) = (%s, %s) ORDER BY s.lesson_number;''', (u_id, day))
    return cursor.fetchall()


@ensure_connection
def show_time_list(con, u_id: int, day: int):
    cursor = con.cursor()
    cursor.execute('''
        SELECT lesson_number FROM schedule 
        WHERE (u_id, day) = (%s, %s) ORDER BY lesson_number;''', (u_id, day))
    return cursor.fetchall()


def code_converter(time: str):
    if time == "8:30" or time == "08:30" or time == "08:30:00":
        return '1'
    if time == "10:15" or time == "10:15:00":
        return '2'
    if time == "12:00" or time == "12:00:00":
        return '3'
    if time == "14:10" or time == "14:10:00":
        return '4'
    if time == "15:55" or time == "15:55:00":
        return '5'
    if time == "17:40" or time == "17:40:00":
        return '6'
    if time == "19:25" or time == "19:25:00":
        return '7'
    else:
        return '0'


if __name__ == '__main__':
    u_id = 73642841
    day = 1
    time = '8:30'
    print(show_schedule(u_id=u_id, day=day))
