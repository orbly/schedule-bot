import telebot
import config
import todolist
import event_db
import schedule_db

mon = 1
tue = 2
wed = 3
thu = 4
fri = 5
sat = 6
sun = 7

client_status = {}


bot = telebot.TeleBot(config.TOKEN)

menu = telebot.types.InlineKeyboardMarkup()
schedule = telebot.types.InlineKeyboardButton(text='Расписание', callback_data='schedule')
toDoList = telebot.types.InlineKeyboardButton(text='TO-DO List', callback_data='toDoList')
events = telebot.types.InlineKeyboardButton(text='Мероприятия', callback_data='events')
menu.add(schedule)
menu.add(toDoList)
menu.add(events)

toDoKeyboard = telebot.types.InlineKeyboardMarkup()
addTask = telebot.types.InlineKeyboardButton(text='Добавить задачу', callback_data='addTask')
delTask = telebot.types.InlineKeyboardButton(text='Удалить задачу', callback_data='delTask')
goBack = telebot.types.InlineKeyboardButton(text='В меню', callback_data='back')
toDoKeyboard.add(addTask)
toDoKeyboard.add(delTask)
toDoKeyboard.add(goBack)

eventsKeyboard = telebot.types.InlineKeyboardMarkup()
addEvent = telebot.types.InlineKeyboardButton(text='Добавить мероприятие', callback_data='addEvent')
delEvent = telebot.types.InlineKeyboardButton(text='Удалить мероприятие', callback_data='delEvent')
goBack = telebot.types.InlineKeyboardButton(text='В меню', callback_data='back')
eventsKeyboard.add(addEvent)
eventsKeyboard.add(delEvent)
eventsKeyboard.add(goBack)

toDoSubKeyboard = telebot.types.InlineKeyboardMarkup()
toDoCancel = telebot.types.InlineKeyboardButton(text='Отмена', callback_data='toDoList')
toDoSubKeyboard.add(toDoCancel)

eventsSubKeyboard = telebot.types.InlineKeyboardMarkup()
eventsCancel = telebot.types.InlineKeyboardButton(text='Отмена', callback_data='events')
eventsSubKeyboard.add(eventsCancel)

scheduleKeyboard = telebot.types.InlineKeyboardMarkup()
goBack = telebot.types.InlineKeyboardButton(text='В меню', callback_data='back')
Monday = telebot.types.InlineKeyboardButton(text='Пн', callback_data='Monday')
Tuesday = telebot.types.InlineKeyboardButton(text='Вт', callback_data='Tuesday')
Wednesday = telebot.types.InlineKeyboardButton(text='Ср', callback_data='Wednesday')
Thursday = telebot.types.InlineKeyboardButton(text='Чт', callback_data='Thursday')
Friday = telebot.types.InlineKeyboardButton(text='Пт', callback_data='Friday')
Saturday = telebot.types.InlineKeyboardButton(text='Сб', callback_data='Saturday')
scheduleKeyboard.add(Monday, Tuesday, Wednesday, Thursday, Friday, Saturday)
scheduleKeyboard.add(goBack)

monKeyboard = telebot.types.InlineKeyboardMarkup()
addMon = telebot.types.InlineKeyboardButton(text='Добавить', callback_data='addMonday')
delMon = telebot.types.InlineKeyboardButton(text='Удалить', callback_data='delMonday')
scheduleBack = telebot.types.InlineKeyboardButton(text='Назад', callback_data='schedule')
monKeyboard.add(addMon)
monKeyboard.add(delMon)
monKeyboard.add(scheduleBack)

tueKeyboard = telebot.types.InlineKeyboardMarkup()
addTue = telebot.types.InlineKeyboardButton(text='Добавить', callback_data='addTuesday')
delTue = telebot.types.InlineKeyboardButton(text='Удалить', callback_data='delTuesday')
tueKeyboard.add(addTue)
tueKeyboard.add(delTue)
tueKeyboard.add(scheduleBack)

wedKeyboard = telebot.types.InlineKeyboardMarkup()
addWed = telebot.types.InlineKeyboardButton(text='Добавить', callback_data='addWednesday')
delWed = telebot.types.InlineKeyboardButton(text='Удалить', callback_data='delWednesday')
wedKeyboard.add(addWed)
wedKeyboard.add(delWed)
wedKeyboard.add(scheduleBack)

thuKeyboard = telebot.types.InlineKeyboardMarkup()
addThu = telebot.types.InlineKeyboardButton(text='Добавить', callback_data='addThursday')
delThu = telebot.types.InlineKeyboardButton(text='Удалить', callback_data='delThursday')
thuKeyboard.add(addThu)
thuKeyboard.add(delThu)
thuKeyboard.add(scheduleBack)

friKeyboard = telebot.types.InlineKeyboardMarkup()
addFri = telebot.types.InlineKeyboardButton(text='Добавить', callback_data='addFriday')
delFri = telebot.types.InlineKeyboardButton(text='Удалить', callback_data='delFriday')
friKeyboard.add(addFri)
friKeyboard.add(delFri)
friKeyboard.add(scheduleBack)

satKeyboard = telebot.types.InlineKeyboardMarkup()
addSat = telebot.types.InlineKeyboardButton(text='Добавить', callback_data='addSaturday')
delSat = telebot.types.InlineKeyboardButton(text='Удалить', callback_data='delSaturday')
satKeyboard.add(addSat)
satKeyboard.add(delSat)
satKeyboard.add(scheduleBack)

scheduleCancel = telebot.types.InlineKeyboardMarkup()
scheduleCancel.add(scheduleBack)


event_db.init_table()
schedule_db.init_table()
todolist.init_table()


@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id, 'Привет, что ты хочешь сделать?', reply_markup=menu)
    client_status[message.chat.id] = config.NOTHING
    print(message.chat.id)


@bot.message_handler(commands=['about'])
def send_info(message):
    bot.send_message(message.chat.id,
                     'Автор бота - Макогоненко Артем.\n\n'
                     'Telegram: @orbly\n'
                     'Email: orblymusic@gmail.com\n'
                     'VK: vk.com/orblie\n'
                     'IG: instagram.com/orbly')
    client_status[message.chat.id] = config.NOTHING


@bot.message_handler(commands=['help'])
def send_help(message):
    bot.send_message(message.chat.id, config.helpMsg, reply_markup=menu)
    client_status[message.chat.id] = config.NOTHING


@bot.message_handler(content_types=['text'])
def handle_text(message):
    u_id = message.chat.id
    if u_id in client_status and client_status[u_id] == config.NOTHING:
        bot.send_message(message.chat.id, 'Привет, что ты хочешь сделать?', reply_markup=menu)
    if u_id in client_status and client_status[u_id] == config.ADD_TASK:
        task = message.text
        todolist.add_task(u_id=u_id, task=task)
        bot.send_message(u_id, 'Задача добавлена')
        todo = todolist.show_list(u_id=u_id)
        text = '\n'.join([f'{todo_task}' for todo_task, in todo])
        bot.send_message(u_id, 'Список задач:\n\n' + text, reply_markup=toDoKeyboard)
        client_status[u_id] = config.NOTHING
    if u_id in client_status and client_status[u_id] == config.DEL_TASK:
        target = message.text
        todo = todolist.show_list(u_id=u_id)
        text = '\n'.join([f'{todo_task}' for todo_task, in todo])
        if target in text:
            todolist.del_task(u_id=u_id, text=target)
            bot.send_message(u_id, 'Задача удалена')
            todo = todolist.show_list(u_id=u_id)
            text = '\n'.join([f'{todo_task}' for todo_task, in todo])
            bot.send_message(u_id, 'Список задач:\n\n' + text, reply_markup=toDoKeyboard)
            client_status[u_id] = config.NOTHING
        else:
            bot.send_message(u_id, 'Нет такой задачи!')
            bot.send_message(u_id, 'Список задач:\n\n' + text, reply_markup=toDoKeyboard)
            client_status[u_id] = config.NOTHING
    if u_id in client_status and client_status[u_id] == config.ADD_EVENT:
        try:
            main_event = message.text
            list = main_event.split(', ')
            print(list)
            event = list[0]
            time = list[1]
            event_db.add_event(u_id=u_id, event=event, time=time)
        except:
            print('Ошибка ввода')
            bot.send_message(u_id, 'Вы неправильно ввели данные! Попробуйте еще раз')

        event_list = event_db.show_events(u_id=message.chat.id)
        text = '\n'.join([f'{eve}, {time}' for eve, time in event_list])
        bot.send_message(u_id, 'Ваши мероприятия: \n\n' + text, reply_markup=eventsKeyboard)
        client_status[u_id] = config.NOTHING
    if u_id in client_status and client_status[u_id] == config.DEL_EVENT:
        target = message.text
        event_list = event_db.show_event_list(u_id=u_id)
        text = '\n'.join([f'{evt}' for evt, in event_list])
        if target in text:
            event_db.del_event(u_id=u_id, event=target)
            bot.send_message(u_id, 'Задача удалена')
            event_list = event_db.show_events(u_id=u_id)
            text = '\n'.join([f'{eve, time}' for eve, time in event_list])
            bot.send_message(u_id, 'Ваши мероприятия: \n\n' + text, reply_markup=eventsKeyboard)
            client_status[u_id] = config.NOTHING
        else:
            bot.send_message(u_id, 'Нет такой задачи!')
            bot.send_message(u_id, 'Список мероприятий:\n\n' + text, reply_markup=eventsKeyboard)
            client_status[u_id] = config.NOTHING
    if u_id in client_status and client_status[u_id] == config.ADD_MON:
        class_parser(message.text, u_id, mon)
        bot.send_message(u_id, 'Пары в понедельник: \n\n' + show_class(u_id, mon), reply_markup=monKeyboard)
        client_status[u_id] = config.NOTHING
    if u_id in client_status and client_status[u_id] == config.ADD_TUE:
        class_parser(message.text, u_id, tue)
        bot.send_message(u_id, 'Пары во вторник: \n\n' + show_class(u_id, tue), reply_markup=tueKeyboard)
        client_status[u_id] = config.NOTHING
    if u_id in client_status and client_status[u_id] == config.ADD_WED:
        class_parser(message.text, u_id, wed)
        bot.send_message(u_id, 'Пары в среду: \n\n' + show_class(u_id, wed), reply_markup=wedKeyboard)
        client_status[u_id] = config.NOTHING
    if u_id in client_status and client_status[u_id] == config.ADD_THU:
        class_parser(message.text, u_id, thu)
        bot.send_message(u_id, 'Пары в четверг: \n\n' + show_class(u_id, thu), reply_markup=thuKeyboard)
        client_status[u_id] = config.NOTHING
    if u_id in client_status and client_status[u_id] == config.ADD_FRI:
        class_parser(message.text, u_id, fri)
        bot.send_message(u_id, 'Пары в пятницу: \n\n' + show_class(u_id, fri), reply_markup=friKeyboard)
        client_status[u_id] = config.NOTHING
    if u_id in client_status and client_status[u_id] == config.ADD_SAT:
        class_parser(message.text, u_id, sat)
        bot.send_message(u_id, 'Пары в субботу: \n\n' + show_class(u_id, sat), reply_markup=satKeyboard)
        client_status[u_id] = config.NOTHING
    if u_id in client_status and client_status[u_id] == config.DEL_MON:
        class_del(message.text, u_id, mon, 'Пары в понедельник', monKeyboard)
        client_status[u_id] = config.NOTHING
    if u_id in client_status and client_status[u_id] == config.DEL_TUE:
        class_del(message.text, u_id, tue, 'Пары во вторник', tueKeyboard)
        client_status[u_id] = config.NOTHING
    if u_id in client_status and client_status[u_id] == config.DEL_WED:
        class_del(message.text, u_id, wed, 'Пары в среду', wedKeyboard)
        client_status[u_id] = config.NOTHING
    if u_id in client_status and client_status[u_id] == config.DEL_THU:
        class_del(message.text, u_id, thu, 'Пары в четверг', thuKeyboard)
        client_status[u_id] = config.NOTHING
    if u_id in client_status and client_status[u_id] == config.DEL_FRI:
        class_del(message.text, u_id, fri, 'Пары в пятницу', friKeyboard)
        client_status[u_id] = config.NOTHING
    if u_id in client_status and client_status[u_id] == config.DEL_SAT:
        class_del(message.text, u_id, sat, 'Пары в субботу', satKeyboard)
        client_status[u_id] = config.NOTHING


def class_del(data, u_id, day, msg_text, markup):
    target = data
    lesson_num = schedule_db.code_converter(target)
    time_list = schedule_db.show_time_list(u_id=u_id, day=day)
    text = '\n'.join([f'{time}' for time, in time_list])
    if lesson_num in text:
        schedule_db.del_class(u_id=u_id, day=day, lesson_num=lesson_num)
        bot.send_message(u_id, 'Пара удалена.')
        bot.send_message(u_id, msg_text + ': \n\n' + show_class(u_id, day), reply_markup=markup)
    else:
        bot.send_message(u_id, 'Пара отсутствует!')
        bot.send_message(u_id, msg_text + ': \n\n' + show_class(u_id, day), reply_markup=markup)


def class_parser(data, u_id, day):
    try:
        main_class = data
        lst = main_class.split(', ')
        lesson_num = lst[0]
        hall = lst[1]
        lesson = lst[2]
        schedule_db.add_class(u_id=u_id, day=day, lesson_num=lesson_num, lecture_hall=hall, lesson=lesson)
    except:
        print('Ошибка ввода')
        bot.send_message(u_id, 'Вы неправильно ввели данные! Попробуйте еще раз')


def show_class(u_id, day):
    class_list = schedule_db.show_schedule(u_id=u_id, day=day)
    text = '\n'.join([f'{time} - {cl}, {au}' for time, cl, au in class_list])
    return text


@bot.callback_query_handler(func=lambda call: True)
def callback_logic(call):
    if call.data == 'toDoList':
        todo = todolist.show_list(u_id=call.message.chat.id)
        text = '\n'.join([f'{todo_task}' for todo_task, in todo])
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Список твоих дел: \n\n' + text, reply_markup=toDoKeyboard)
        client_status[call.message.chat.id] = config.NOTHING
    elif call.data == 'events':
        event_list = event_db.show_events(u_id=call.message.chat.id)
        text = '\n'.join([f'{eve}, {time}' for eve, time in event_list])
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Список мероприятий: \n\n' + text, reply_markup=eventsKeyboard)
        client_status[call.message.chat.id] = config.NOTHING
    elif call.data == 'schedule':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Выберите день недели', reply_markup=scheduleKeyboard)
        client_status[call.message.chat.id] = config.NOTHING
    elif call.data == 'back':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Главное меню', reply_markup=menu)
        client_status[call.message.chat.id] = config.NOTHING
    elif call.data == 'addTask':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите название задачи', reply_markup=toDoSubKeyboard)
        client_status[call.message.chat.id] = config.ADD_TASK
    elif call.data == 'delTask':
        todo = todolist.show_list(u_id=call.message.chat.id)
        text = '\n'.join([f'{todo_task}' for todo_task, in todo])
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите название задачи, которую хотите удалить\n\n' + text,
                              reply_markup=toDoSubKeyboard)
        client_status[call.message.chat.id] = config.DEL_TASK
    elif call.data == 'addEvent':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите название мероприятия.\nФормат: <мероприятие>, <дата и время>\n\n'
                              + 'Примечание: дату указывать в формате ДД.ММ.ГГ ЧЧ:ММ',
                              reply_markup=eventsSubKeyboard)
        client_status[call.message.chat.id] = config.ADD_EVENT
    elif call.data == 'delEvent':
        event_list = event_db.show_events(u_id=call.message.chat.id)
        text = '\n'.join([f'{eve}, {time}' for eve, time in event_list])
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите название мероприятия, которое хотите удалить\n\n' + text,
                              reply_markup=eventsSubKeyboard)
        client_status[call.message.chat.id] = config.DEL_EVENT
    elif call.data == 'Monday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Пары в понедельник: \n\n' + show_class(call.message.chat.id, mon),
                              reply_markup=monKeyboard)
    elif call.data == 'Tuesday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Пары во вторник: \n\n' + show_class(call.message.chat.id, tue),
                              reply_markup=tueKeyboard)
    elif call.data == 'Wednesday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Пары в среду: \n\n' + show_class(call.message.chat.id, wed),
                              reply_markup=wedKeyboard)
    elif call.data == 'Thursday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Пары в четверг: \n\n' + show_class(call.message.chat.id, thu),
                              reply_markup=thuKeyboard)
    elif call.data == 'Friday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Пары в пятницу: \n\n' + show_class(call.message.chat.id, fri),
                              reply_markup=friKeyboard)
    elif call.data == 'Saturday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Пары в субботу: \n\n' + show_class(call.message.chat.id, sat),
                              reply_markup=satKeyboard)
    elif call.data == 'addMonday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите пару.\nФормат: <номер пары>, <аудитория>, <занятие>\n\n' +
                                   'Примечание: номер пары следует указывать в зависимости от времени ' +
                                   '(напр. пара в 10:15 - это вторая)', reply_markup=scheduleCancel)
        client_status[call.message.chat.id] = config.ADD_MON
    elif call.data == 'addTuesday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите пару.\nФормат: <номер пары>, <аудитория>, <занятие>\n\n' +
                                   'Примечание: номер пары следует указывать в зависимости от времени ' +
                                   '(напр. пара в 10:15 - это вторая)', reply_markup=scheduleCancel)
        client_status[call.message.chat.id] = config.ADD_TUE
    elif call.data == 'addWednesday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите пару.\nФормат: <номер пары>, <аудитория>, <занятие>\n\n' +
                                   'Примечание: номер пары следует указывать в зависимости от времени ' +
                                   '(напр. пара в 10:15 - это вторая)', reply_markup=scheduleCancel)
        client_status[call.message.chat.id] = config.ADD_WED
    elif call.data == 'addThursday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите пару.\nФормат: <номер пары>, <аудитория>, <занятие>\n\n' +
                                   'Примечание: номер пары следует указывать в зависимости от времени ' +
                                   '(напр. пара в 10:15 - это вторая)', reply_markup=scheduleCancel)
        client_status[call.message.chat.id] = config.ADD_THU
    elif call.data == 'addFriday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите пару.\nФормат: <номер пары>, <аудитория>, <занятие>\n\n' +
                                   'Примечание: номер пары следует указывать в зависимости от времени ' +
                                   '(напр. пара в 10:15 - это вторая)', reply_markup=scheduleCancel)
        client_status[call.message.chat.id] = config.ADD_FRI
    elif call.data == 'addSaturday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите пару.\nФормат: <номер пары>, <аудитория>, <занятие>\n\n' +
                                   'Примечание: номер пары следует указывать в зависимости от времени ' +
                                   '(напр. пара в 10:15 - это вторая)', reply_markup=scheduleCancel)
        client_status[call.message.chat.id] = config.ADD_SAT
    elif call.data == 'delMonday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите время пары, которую хотите убрать\n\n'
                                   + show_class(call.message.chat.id, mon), reply_markup=scheduleCancel)
        client_status[call.message.chat.id] = config.DEL_MON
    elif call.data == 'delTuesday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите время пары, которую хотите убрать\n\n'
                                   + show_class(call.message.chat.id, tue), reply_markup=scheduleCancel)
        client_status[call.message.chat.id] = config.DEL_TUE
    elif call.data == 'delWednesday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите время пары, которую хотите убрать\n\n'
                                   + show_class(call.message.chat.id, wed), reply_markup=scheduleCancel)
        client_status[call.message.chat.id] = config.DEL_WED
    elif call.data == 'delThursday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите время пары, которую хотите убрать\n\n'
                                   + show_class(call.message.chat.id, thu), reply_markup=scheduleCancel)
        client_status[call.message.chat.id] = config.DEL_THU
    elif call.data == 'delFriday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите время пары, которую хотите убрать\n\n'
                                   + show_class(call.message.chat.id, fri), reply_markup=scheduleCancel)
        client_status[call.message.chat.id] = config.DEL_FRI
    elif call.data == 'delSaturday':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Введите время пары, которую хотите убрать\n\n'
                                   + show_class(call.message.chat.id, sat), reply_markup=scheduleCancel)
        client_status[call.message.chat.id] = config.DEL_SAT


if __name__ == '__main__':
    bot.polling(none_stop=True)
