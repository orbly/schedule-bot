import psycopg2
import os

DATABASE_URL = os.environ['DATABASE_URL']


def ensure_connection(func):
    def inner(*args, **kwargs):
        with psycopg2.connect(DATABASE_URL, sslmode='require') as con:
            res = func(*args, con=con, **kwargs)
        return res
    return inner


@ensure_connection
def init_table(con, force: bool = False):
    c = con.cursor()

    if force:
        c.execute('DROP TABLE IF EXISTS to_do_list;')

    c.execute('''
        CREATE TABLE IF NOT EXISTS to_do_list (
            id serial PRIMARY KEY,
            u_id integer NOT NULL,
            task text NOT NULL
        );
    ''')
    con.commit()


@ensure_connection
def add_task(con, u_id: int, task: str):
    cursor = con.cursor()
    cursor.execute('INSERT INTO to_do_list (u_id, task) VALUES (%s, %s);', (u_id, task))
    con.commit()


@ensure_connection
def del_task(con, u_id: int, text: str):
    cursor = con.cursor()
    cursor.execute('DELETE FROM to_do_list WHERE (task, u_id) = (%s, %s);', (text, u_id))
    con.commit()


@ensure_connection
def show_list(con, u_id: int):
    cursor = con.cursor()
    cursor.execute('SELECT task FROM to_do_list WHERE u_id = %s ORDER BY id DESC', (u_id,))
    return cursor.fetchall()


if __name__ == '__main__':
    init_table()
    li = show_list(u_id=1234)
    text = '\n'.join([f'{task}' for task, in li])
    print(text)
